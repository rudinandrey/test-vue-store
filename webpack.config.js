const path = require('path');
module.exports = {
	entry: {
		'main': './src/main.js',
		'test': './src/test.js'
	},
	optimization: {
		chunkIds: 'named',
		splitChunks: {
			cacheGroups: {
				commons: {
					chunks: 'initial',
					minChunks: 2,
					maxInitialRequests: 5,
					minSize: 0
				},
				vendor: {
					test: /node_modules/,
					chunks: 'initial',
					name: 'vendor',
					priority: 10,
					enforce: true
				}
			}
		}
	},
	output: {
		path: path.join(__dirname, 'dist'),
		filename: "[name].js"
	}
}