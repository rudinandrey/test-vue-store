import { createStore } from 'vuex';

const storeKey = Symbol.for('application-store');

const store = createStore({
	state() {
		return {
			num: Math.random()
		}
	},
	mutations: {
		change(state) {
			state.num = 100;
		}
	},
	actions: {
		change(context) {
			context.commit('change');
		}
	}
});

export default function makeStore() {
	if (!window[storeKey]) {
		window[storeKey] = store;
	}
	return window[storeKey];
}