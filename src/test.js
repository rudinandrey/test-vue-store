import {createApp} from 'vue'
import makeStore from './store';
import Test from './Test.vue';

createApp(Test).use(makeStore()).mount('#test');