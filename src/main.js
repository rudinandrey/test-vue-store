import { createApp } from 'vue'
import makeStore from './store';
import App from './App.vue'

createApp(App).use(makeStore()).mount('#app')