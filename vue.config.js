const path = require('path');

module.exports = {
	filenameHashing: false,
	pages: {
		index: {
			entry: 'src/main.js',
			chunks: ['chunk-vendors', 'chunk-common', 'main']
		},
		test: {
			entry: 'src/test.js',
			chunks: ['chunk-vendors', 'chunk-common', 'test']
		}
	}
}